/**
 * Created with IntelliJ IDEA.
 * User: Elliott
 * Date: 9/5/13
 * Time: 1:11 PM
 * To change this template use File | Settings | File Templates.
 */
package selectors {


import com.greensock.TimelineLite;
import com.greensock.TweenLite;
import com.greensock.TweenMax;
import com.greensock.easing.Expo;

import flash.display.Bitmap;
import flash.display.Shape;

import flash.display.Sprite;
import flash.display3D.textures.Texture;
import flash.events.Event;

import flash.geom.Rectangle;

import flash.geom.Rectangle;
import flash.text.AntiAliasType;
import flash.text.Font;
import flash.text.StyleSheet;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;


public class NumberSelector extends Sprite {


    //---------------------------------------------------------------
    //  Private variables
    //---------------------------------------------------------------
    private var _bg:Shape;
    private var _digits:Vector.<TextField>;
    private var _digitToEdit:Number = -1;
    private var _textWidth:Number;
    private var _animating:Boolean;
    private var _isShakeing:Boolean;
    private var _bgX:Number;
    private var _centralised:Boolean;
    private var _leftAmount:Number;
    private var _rightAmount:Number;
    private var _expanding:Boolean;
    private var _isCurrentNumberSelector:Boolean;
    private var _format:TextFormat;
    private var _formatBig:TextFormat;
    private var _startFormat:TextFormat;
    private var _format1:TextFormat;
    private var _format2:TextFormat;
    private var _digit1:Number;
    private var _digit2:Number;
    private var _colour:Boolean;

    public function NumberSelector(background:Bitmap,rect:Rectangle, font:Font) {
        _startFormat = new TextFormat();
        _startFormat.color = 0xFFFFFF;
        _startFormat.font = font.fontName;
        _startFormat.size = 1;
        _startFormat.align = TextFormatAlign.CENTER;

        _format = new TextFormat();
        _format.color = 0xFFFFFF;
        _format.font = font.fontName;
        _format.size = 82;
        _format.align = TextFormatAlign.CENTER;

        _formatBig = new TextFormat();
        _formatBig.color = 0xFFFFFF;
        _formatBig.font = font.fontName;
        _formatBig.size = 122;
        _formatBig.align = TextFormatAlign.CENTER;

        _format1 = new TextFormat();
        _format1.color = 0xFFFFFF;
        _format1.font = font.fontName;
        _format1.size = 82;
        _format1.align = TextFormatAlign.CENTER;

        _format2 = new TextFormat();
        _format2.color = 0xFFFFFF;
        _format2.font = font.fontName;
        _format2.size = 122;
        _format2.align = TextFormatAlign.CENTER;
        _bg = new Shape();
        _bg.graphics.lineStyle(2, 0xFFFFFF);
        _bg.graphics.drawRoundRect(0,0,250,250,115,115);
        _bg.graphics.endFill();
        _bg.scale9Grid = new Rectangle(70,70, 100, 100)

        addChild(_bg);

        _digits = new Vector.<TextField>();
        _textWidth = _bg.width;
        addDigit(0, false);
        _bgX = 0;
        _isShakeing = false;
        _centralised = false;
        _expanding = false;
        _isCurrentNumberSelector = true;
        _leftAmount = 0;
        _rightAmount = 0;
        enable();

        _colour=true;
    }

    public function changeSize1():void {
        this.addEventListener(Event.ENTER_FRAME, onFrame1);
        TweenLite.to(_format1, 0.3,{size:122, onComplete:removeFrame1});
    }

    private function onFrame1(event:Event):void {
        _digits[_digit1].setTextFormat(_format1);
        _digits[_digit1].y = _bg.height - _digits[_digit1].height >> 1;
    }

    private function removeFrame1():void {
        this.removeEventListener(Event.ENTER_FRAME, onFrame1);
        _digits[_digit1].defaultTextFormat = _formatBig;
        _format1.size = 82;

    }

    public function changeSize2(_size:Number = 82):void {
        this.addEventListener(Event.ENTER_FRAME, onFrame2);
        TweenLite.to(_format2, 0.3,{size:_size, onComplete:removeFrame2});
    }

    private function onFrame2(event:Event):void {
        _digits[_digit2].setTextFormat(_format2);
        _digits[_digit2].y = _bg.height - _digits[_digit2].height >> 1;
    }

    private function removeFrame2():void {
        this.removeEventListener(Event.ENTER_FRAME, onFrame2);
        _digits[_digit2].defaultTextFormat = _format;
        _format2.size = 122;
    }




    public function addDigit(number:Number, animate:Boolean = true):void {
        var duration:Number;
        if (animate)
            duration = 0.3;
        else
            duration = 0;
        if (_digits.length < 4 && _digitToEdit == _digits.length-1 && !_animating) {
            disable();
            var _length:Number = _digits.length;
            for (var i:int = 0; i < _length; i++) {
                TweenLite.to(_digits[i], duration, {x : _digits[i].x - 77});
            }
            if (_length > 0 && animate) {
                updateBackground(77, "left");
                TweenLite.to(_digits[_digitToEdit], duration, { alpha:0.6});
                _digit2 = _digitToEdit;
                changeSize2();

            }
            else if (_length > 0 && !animate) {
                updateBackground(77, "left", 0);
                TweenLite.to(_digits[_digitToEdit], duration, { alpha:0.6});
                _digit2 = _digitToEdit;
                changeSize2();

            }

            var newText:TextField = new TextField();
            newText.setTextFormat(_startFormat);
            newText.antiAliasType = AntiAliasType.ADVANCED;
            newText.condenseWhite = true;
            newText.wordWrap = true;
            newText.autoSize = TextFieldAutoSize.CENTER;
            newText.width = _textWidth;
            newText.text =  number.toString();
            newText.alpha = 0.6;
            _digits.push(newText);
            _digitToEdit++;
            addChild(newText);
            TweenLite.to(newText,duration, {alpha:1, onComplete:enable});
            _digit1 = _digitToEdit;
            changeSize1();
            _leftAmount = 0;
        }
    }

    public function moveDigitsLeft():void {
        if (_digits.length > _digitToEdit+1 && !_animating) {
            disable();

            var _length:Number = _digits.length;
            for (var i:int = 0; i < _length; i++) {
                TweenLite.to(_digits[i], 0.3, {x : _digits[i].x - 77});
            }
            TweenLite.to(_digits[_digitToEdit], 0.27, { alpha:0.6, onComplete:enable});
            TweenLite.to(_digits[_digitToEdit+1], 0.3, {alpha:1});
            _digit1 = _digitToEdit+1;
            _digit2 = _digitToEdit;
            changeSize1();
            changeSize2();
            TweenLite.to(_bg, 0.3, { x:_bg.x - 77, onComplete:additionalLeftMove});
            _digitToEdit++;
            if (_centralised)
                TweenLite.to(this, 0.3, {x : this.x + 77})
        }
        else if (_digits.length > _digitToEdit+1 && !_animating) {
            _leftAmount++;
        }
        else {
            if (parseInt(_digits[0].text) <= 0 && !_animating)
                _digits[0].text = "1";
            addDigit(0);
        }

    }

    public function additionalLeftMove():void {

        if (_leftAmount > 0) {
            _leftAmount--;
            moveDigitsLeft();
        }
        else if (_rightAmount > 0) {
            _rightAmount--;
            moveDigitsRight();
        }

    }

    public function moveDigitsRight():void {
        if (0 < _digitToEdit && !_animating) {
            disable();
            var _length:Number = _digits.length;
            for (var i:int = 0; i < _length; i++) {
                TweenLite.to(_digits[i], 0.3, {x : _digits[i].x + 77});
            }
            TweenLite.to(_digits[_digitToEdit], 0.27, {  alpha:0.6, onComplete:enable});
            TweenLite.to(_digits[_digitToEdit-1], 0.3, { alpha:1});
            _digit1 = _digitToEdit-1;
            _digit2 = _digitToEdit;
            changeSize1();
            changeSize2();
            TweenLite.to(_bg, 0.3, { x:_bg.x + 77,onComplete:additionalRightMove});
            _digitToEdit--;
            if (_centralised)
                TweenLite.to(this, 0.3, {x : this.x - 77})
        }
        else if (0 < _digitToEdit && !_animating) {
            _rightAmount++;
        }
    }

    public function additionalRightMove():void {

        if (_rightAmount > 0) {
            _rightAmount--;
            moveDigitsRight();
        }
        else if (_leftAmount > 0) {
            _leftAmount--;
            moveDigitsLeft();
        }
    }



    public function centralise(animate:Boolean = true):void {
        var duration:Number;
        if (animate)
            duration = 0.5;
        else
            duration = 0;
       //TweenLite.to(_digits[_digitToEdit], duration, {scaleX:0.8,scaleY:0.8, y: _bg.height - _digits[_digitToEdit].height*0.8 >>1, alpha: 0.6});
       var temp:Number = 0;
       for (var i:int = 0; i < _digitToEdit; i++) {
           temp += 77;
       }

       var length:Number = _digits.length;
       for (var i:int = 0; i < length; i++) {
           TweenLite.to(_digits[i], duration, {x: _digits[i].x + temp})
       }
       TweenLite.to(_bg, duration, {x: _bg.x + temp});
       _bgX += temp;
       _digitToEdit = 0;
       _centralised = true;

    }

    public function expand():void {
        if (!_expanding) {
            _expanding = true;
            _isCurrentNumberSelector = false;
            TweenLite.to(this,0.25,{scaleX : 1.2, scaleY: 1.2});
            var x:Number = this.x;
            var y:Number = this.y;
            TweenLite.to(this,0.25,{x: this.x - _bg.width/10, y: this.y - _bg.height/10, onComplete:retract, onCompleteParams:[x, y]});
        }
    }

    private function retract(x:Number, y:Number):void {
        _bg.graphics.clear();
        if (_colour)
            _bg.graphics.lineStyle(3,0x900000);
        else
            _bg.graphics.lineStyle(2,0xFFFFFF);
        _bg.graphics.drawRoundRect(0,0,250,250,115,115);
        _bg.graphics.endFill();
        _colour = !_colour;
        TweenLite.to(this,0.25,{scaleX : 1, scaleY: 1});

        TweenLite.to(this,0.25,{x: x, y: y, onComplete:onRetract()});



    }

    private function onRetract():void {
        _expanding = false;
        _isCurrentNumberSelector = true;
    }

    public function removeCurrentDigit():void {
        if (_digits.length == _digitToEdit+1 && _digits.length > 1 && !_animating) {
            disable();
            var _length:Number = _digits.length;
            for (var i:int = 0; i < _length-1; i++) {
                TweenLite.to(_digits[i], 0.3, {x : _digits[i].x + 77});
            }

            _digit1 = _digitToEdit-1;
            _digit2 = _digitToEdit;
            changeSize1();
            changeSize2(1);
            TweenLite.to(_digits[_digitToEdit-1], 0.3, { alpha:1, onComplete:enable});
            TweenLite.to(_digits[_digitToEdit], 0.3, { alpha:0.6,  onComplete:onRemoveDigit, onCompleteParams:[1]});
            updateBackground(-77,"right");
            _digitToEdit--;
        }
        else if (_digitToEdit != 0 &&_digits.length > 1 && !_animating) {
            disable();
            var _length:Number = _digits.length;
            for (var i:int = _digitToEdit+1; i < _length; i++) {
                TweenLite.to(_digits[i], 0.3, {x : _digits[i].x - 77});
            }
            _digit1 = _digitToEdit+1;
            _digit2 = _digitToEdit;
            changeSize1();
            changeSize2(1);
            TweenLite.to(_digits[_digitToEdit+1], 0.3, {alpha:1, onComplete:enable});
            TweenLite.to(_digits[_digitToEdit], 0.3, { alpha:0.6, onComplete:onRemoveDigit, onCompleteParams:[2]});
            updateBackground(-77,"left");
        }
        else if (_digitToEdit == 0 && digits.length >1 && !_animating) {
            disable();
            var _length:Number = _digits.length;
                for (var i:int = _digitToEdit+1; i < _length; i++) {
                    TweenLite.to(_digits[i], 0.3, {x : _digits[i].x - 77});
                }
            _digit1 = _digitToEdit+1;
            _digit2 = _digitToEdit;
            changeSize1();
            changeSize2(1);
            TweenLite.to(_digits[_digitToEdit+1], 0.3, {alpha:1, onComplete:enable});
            TweenLite.to(_digits[_digitToEdit], 0.3, {alpha:0.6, onComplete:onRemoveDigit, onCompleteParams:[4]});
            updateBackground(-77,"left");
        }

        else if (_digitToEdit == 0 && !_animating) {
            _digits[_digitToEdit].text = "0";
        }
    }
    
    public function reset():void {
        if (_digits.length > 1 && _digitToEdit != 0) {
            removeDigitFront(true);
        }
        else if (_digits.length > 1) {
            disable();
            var _length:Number = _digits.length;
            for (var i:int = _digitToEdit+1; i < _length; i++) {
                TweenLite.to(_digits[i], 0.3, {x : _digits[i].x - 77});
            }
            TweenLite.to(_digits[_digitToEdit+1], 0.3, { alpha:1});
            TweenLite.to(_digits[_digitToEdit], 0.3, { alpha:0.6, onComplete:reset});
            _digit1 = _digitToEdit+1;
            _digit2 = _digitToEdit;
            changeSize1();
            changeSize2(1);
            updateBackground(-77,"left");
        }
        else {
            _digits[_digitToEdit].text =  "0" ;
            enable();
        }
    }

    public function shakeTween():void
    {
        if (!_isShakeing && !_animating) {
            TweenLite.to(_digits[_digitToEdit], 0.3, { alpha:0.6});
            _digit2 = _digitToEdit;
            changeSize2();
            _bgX = _bg.x;
            TweenMax.to(_bg,0.15,{repeat:-1, y:_bg.y+(5), x:_bg.x+(5), delay:0.1, ease:Expo.easeInOut});
            TweenMax.to(_bg,0.15,{y:_bg.y, x:_bg.x, ease:Expo.easeInOut});
            _isShakeing = true;
            disable();

        }
    }

    public function stopShake():void {
        if (_isShakeing) {
            if (_isCurrentNumberSelector) {
                TweenLite.to(_digits[_digitToEdit], 0.3, {alpha:1});
                _digit1 = _digitToEdit;
                changeSize1();
            }
            TweenLite.killTweensOf(_bg);
            _bg.x = _bgX;
            _bg.y = 0;
            _isShakeing = false;
            enable();
        }
    }

    private function onRemoveDigit(i:int):void {
        if (i == 1) {
            removeChild(_digits[_digitToEdit+1]);
            _digits.pop();
        }
        else if (i == 2) {
            removeChild(_digits[_digitToEdit]);
            _digits.splice(_digitToEdit,1);
        }
        else if (i ==3) {
            removeChild(_digits[0]);
            _digits.splice(0,1);
            enable();
            if (_digits[_digitToEdit].text == "0" && digits.length > 1)
                removeDigitFront();
        }
        else if (i ==4) {
            removeChild(_digits[0]);
            _digits.shift();
            if (_digits[0].text == "0" && _digitToEdit == 0) {
                removeCurrentDigit();
            }
        }
        else if (i ==5) {
            removeChild(_digits[0]);
            _digits.splice(0,1);
            enable();
            reset();
        }
    }

    private function disable():void {
        _animating = true;
    }

    private function enable():void {
        _animating = false;
    }

    public function set isCurrentNumberSelector(value:Boolean):void {
        _isCurrentNumberSelector = value;
    }

    public function get animating():Boolean {
        return _animating;
    }

    public function get bg():Shape {
        return _bg;
    }

    public function get isShakeing():Boolean {
        return _isShakeing;
    }

    public function get digitToEdit():Number {
        return _digitToEdit;
    }

    public function get digits():Vector.<TextField> {
        return _digits;
    }


    public function set centralised(value:Boolean):void {
        _centralised = value;
    }

    private function updateBackground(size:Number, direction:String, duration:Number = 0.3):void {

        if (direction == "left" && size > 0) {
            TweenLite.to( _bg,duration, {width:_bg.width + size, x:_bg.x - size });
            _bgX -= size;
            if (_centralised)
                TweenLite.to(this, duration, {x : this.x +size/2})
        }
        else if (direction == "left" && size < 0) {
            TweenLite.to( _bg,duration, {width:_bg.width + size});
            _bgX += size;
                if (_centralised)
                    TweenLite.to(this, duration, {x : this.x -size/2})
        }
        else if (direction == "right" && size > 0) {
            TweenLite.to( _bg,duration, {width:_bg.width + size, x:_bg.x + size });
            _bgX += size;
            if (_centralised)
               TweenLite.to(this, duration, {x : this.x +size/2})
        }
        else if (direction == "right" && size < 0) {
            TweenLite.to( _bg,duration, {width:_bg.width + size, x:_bg.x + size*-1});
            _bgX += size*-1;
            if (_centralised)
                TweenLite.to(this, duration, {x : this.x +size/2})
        }

    }

    public function onDigitChange(number:int, digitToChange:Number):void {
        if (!_isShakeing && !_expanding) {
            var numberChange:int = parseInt(_digits[digitToChange].text);
            numberChange = numberChange + number;
            if (numberChange > 9 && digitToChange != 0) {
                numberChange = numberChange - 10;
                _digits[digitToChange].text =  numberChange.toString();
                onDigitChange(1, digitToChange-1);
            }
            else if (numberChange > 9 && digitToChange == 0) {
                numberChange = numberChange - 10;
                _digits[digitToChange].text = numberChange.toString();
                extraDigitFront();
            }
            else if (numberChange < 0 && digitToChange != 0) {
                _digits[digitToChange].text = "9" ;
                onDigitChange(-1,digitToChange-1);
            }
            else if (numberChange < 1 && digitToChange == 0 && _digits.length > 1) {
                if (_digitToEdit == 0)
                    removeDigitFront();
                else
                    removeDigitFront();
            }
            else if (numberChange < 0 && digitToChange == 0 && _digits.length == 1) {
                _digits[digitToChange].text = "0" ;
            }
            else {
                _digits[digitToChange].text =  numberChange.toString();
            }
        }

    }

    private function removeDigitFront(onReset:Boolean = false):void {
        if (!_animating) {
            disable();
            var direction:String = "right";
            if (_digitToEdit == 0) {
                direction = "left";
                var _length:Number = _digits.length;
                for (var i:int = 1; i < _length; i++) {
                    TweenLite.to(_digits[i], 0.3, {x : _digits[i].x - 77});
                }
                TweenLite.to(_digits[1], 0.3, { alpha:1});
                _digit1 = 1;
                changeSize1();
            }
            else {
                _digitToEdit--;
            }
            if (onReset) {
                TweenLite.to(_digits[0], 0.3, { alpha:0.6, onCompleteParams:[5]});
                _digit2 = 0;
                changeSize2();
            }
            else  {
                TweenLite.to(_digits[0], 0.3, { alpha:0.6, onCompleteParams:[3]});
                _digit2 = 0;
                changeSize2();
            }

            updateBackground(-77,direction);
        }
    }

    public function extraDigitFront():void {
        if (!_animating && _digits.length < 4) {
            disable();
            updateBackground(77, "left");
            var newText:TextField = new TextField();
            newText.setTextFormat(_startFormat);
            newText.antiAliasType = AntiAliasType.ADVANCED;
            newText.condenseWhite = true;
            newText.wordWrap = true;
            newText.autoSize = TextFieldAutoSize.CENTER;
            newText.width = _textWidth;
            newText.text =  "1";
            newText.alpha = 0.6;
            newText.x = digits[0].x -77;
            _digits.unshift(newText);
            _digitToEdit++;
            _digit2 =  0;
            changeSize2();
            addChild(newText);
            enable();
        }

    }

    public function partyCount():Number {
        var partyCount:String = "";
        for (var i:int = 0; i < _digits.length; i++) {
            partyCount = partyCount + _digits[i].text;
        }
        return parseInt(partyCount);
    }

    public function setPartyCount(count:Number):void {
        var countString:String = count.toString();
        _digits[0].text = countString.charAt(0);
        for (var i:int = 1; i < countString.length; i++) {
            addDigit(parseInt(countString.charAt(i)),false);
        }
    }
}
}
