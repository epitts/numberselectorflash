package {

import flash.display.Bitmap;
import flash.display.Sprite;
import flash.geom.Rectangle;
import flash.system.Capabilities;
import flash.text.AntiAliasType;
import flash.text.Font;
import flash.text.StyleSheet;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;



import org.gestouch.events.GestureEvent;
import org.gestouch.gestures.LongPressGesture;
import org.gestouch.gestures.SwipeGesture;
import org.gestouch.gestures.SwipeGestureDirection;
import org.gestouch.gestures.TapGesture;
import org.gestouch.gestures.ZoomGesture;
import org.osflash.signals.Signal;

import selectors.NumberSelector;

public class PartySelectionView extends Sprite {

    //---------------------------------------------------------------
    //  Public variables
    //---------------------------------------------------------------
    public var twoFingerSwipeLeft:Signal;

    //---------------------------------------------------------------
    //  Private variables
    //---------------------------------------------------------------

    [Embed(source="../media/fonts/alacarteWaiterStyle.css", mimeType="application/octet-stream")]
    private var stylesCSS:Class;

    [Embed(source="../media/graphics/Box.png")]
    public static const Background:Class;

    [Embed(source="../media/graphics/BoxRed.png")]
    public static const BackgroundAlt:Class;

    private var _titleTextField: TextField;
    private var _subTitleTextField: TextField;
    private var _numberSelectorBackground:Bitmap;
    private var _altBg:Bitmap;
    private var _rectangle:Rectangle;

    private var _currentNumberSelector:Number;

    private var _numberSelectors:Vector.<NumberSelector>;
    private var _oneFingerSwipeLeftGesture:SwipeGesture;
    private var _oneFingerSwipeRightGesture:SwipeGesture;
    private var _oneFingerSwipeUpGesture:SwipeGesture;
    private var _oneFingerSwipeDownGesture:SwipeGesture;
    private var _twoFingerSwipeDownGesture:SwipeGesture;
    private var _twoFingerSwipeLeftGesture:SwipeGesture;
    private var _oneFingerTapGesture:TapGesture;
    private var _twoFingerTapGesture:TapGesture;
    private var _threeFingerTapGesture:TapGesture;
    private var _fourFingerTapGesture:TapGesture;
    private var _pinchInGesture:ZoomGesture;
    private var _textFormat:TextFormat;
    public var _css:StyleSheet;
    public var _font:Font
    private var _format:TextFormat;
    private var _formatSub:TextFormat;

    public function PartySelectionView() {

        _titleTextField = new TextField();
        _subTitleTextField = new TextField();

        twoFingerSwipeLeft = new Signal();

    }


    //---------------------------------------------------------------
    //  Public methods
    //---------------------------------------------------------------

    public function init(_label:String,font:Font, subLabel:String = ""):void
    {

        var fonts:Array = Font.enumerateFonts();
        for each (var fontana:Font in fonts) {
            trace(fontana.fontName);
        }
        _font = font;
        _format = new TextFormat();
        _format.color = 0xCCCCCC;
        _format.font = "HelveticaNeueUltraLight";
        _format.size = 82;
        _format.align = TextFormatAlign.CENTER;

        _formatSub = new TextFormat();
        _formatSub.color = 0xCCCCCC;
        _formatSub.font = "HelveticaNeueUltraLight";
        _formatSub.size = 45;
        _formatSub.align = TextFormatAlign.CENTER;
        initGestures();
        graphics.beginFill(0x151515);
        graphics.drawRect(0,0,1136, 640);

        _numberSelectorBackground = newBackground();
        _altBg = newBackground(true);
        var _numberSelector = new NumberSelector(_numberSelectorBackground, new Rectangle( 55,55, 138, 138),font);
        _numberSelector.y = this.height/3 - _numberSelector.height/2;
        _numberSelector.x = this.width - _numberSelector.width >>1;
        addChild(_numberSelector);


        _numberSelectors = new Vector.<NumberSelector>();
        _numberSelectors.push(_numberSelector);
        _currentNumberSelector = 0;
        _titleTextField.defaultTextFormat = _format;
        _titleTextField.antiAliasType = AntiAliasType.ADVANCED;
        _titleTextField.condenseWhite = true;
        _titleTextField.autoSize = TextFieldAutoSize.CENTER;
        _titleTextField.y = this.height - 250;
        _titleTextField.alpha = 0.7;
        addChild(_titleTextField);

        _subTitleTextField.y = this.height - 150;
        _subTitleTextField.defaultTextFormat = _formatSub;
        _subTitleTextField.antiAliasType = AntiAliasType.ADVANCED;
        _subTitleTextField.condenseWhite = true;
        _subTitleTextField.autoSize = TextFieldAutoSize.CENTER;
        _subTitleTextField.alpha = 0.7;

        addChild(_subTitleTextField);

        titleText = _label;

        subTitleText = subLabel;

        enable();

    }


    public function get format():TextFormat {
        return _format;
    }

    public function get formatSub():TextFormat {
        return _formatSub;
    }

    public function newBackground(alt:Boolean = false):Bitmap {
        if (!alt)
            var bg:Bitmap = new Background();
        else
            var bg:Bitmap = new BackgroundAlt();
        return bg;
    }

    public function createStyleSheetObject():StyleSheet
    {
        // create a new StyleSheet instance
        var styleSheet:StyleSheet = new StyleSheet();
        styleSheet.parseCSS(new stylesCSS().toString());
        return styleSheet;
    }

    public function reset():void {
        for (var i:int = 0; i < numberSelectors.length; i++) {
            _numberSelectors[i].reset();
        }
    }
//    public function setTextures(background:Texture, rect:Rectangle, altBg:Texture = null):void {
//        _numberSelectorBackground = background;
//
//        _rectangle = rect;
//        _altBg = altBg;
//    }

    public function set titleText(value:String):void {
        _titleTextField.text =  value.toString() ;
        _titleTextField.x = this.width - _titleTextField.width >>1;
    }

    public function set subTitleText(value:String):void {
        _subTitleTextField.text =   value.toString() ;
        _subTitleTextField.x = this.width - _subTitleTextField.width >>1;
    }


    public function get titleTextField():TextField {
        return _titleTextField;
    }

    public function enable():void {
        visible = mouseEnabled = true;
        addGestureListeners();
    }

    public function disable():void {
        visible = mouseEnabled = false;
        removeGestureListeners();
    }

    public function get currentNumberSelector():Number {
        return _currentNumberSelector;
    }

    public function set currentNumberSelector(value:Number):void {
        _currentNumberSelector = value;
    }

    public function get numberSelectors():Vector.<NumberSelector> {
        return _numberSelectors;
    }


    public function get subTitleTextField():TextField {
        return _subTitleTextField;
    }

    public function get altBg():Bitmap {
        return _altBg;
    }

    public function get numberSelectorBackground():Bitmap {
        return _numberSelectorBackground;
    }

    public function get rectangle():Rectangle {
        return _rectangle;
    }

    public function partyCount():Number {
        return _numberSelectors[_currentNumberSelector].partyCount();
    }

    protected function initGestures():void {
        _oneFingerSwipeLeftGesture= new SwipeGesture(this);
        _oneFingerSwipeLeftGesture.direction = SwipeGestureDirection.LEFT;
        _oneFingerSwipeLeftGesture.numTouchesRequired = 1;

        _oneFingerSwipeRightGesture = new SwipeGesture(this);
        _oneFingerSwipeRightGesture.direction = SwipeGestureDirection.RIGHT;
        _oneFingerSwipeRightGesture.numTouchesRequired = 1;

        _oneFingerSwipeUpGesture= new SwipeGesture(this);
        _oneFingerSwipeUpGesture.direction = SwipeGestureDirection.UP;
        _oneFingerSwipeUpGesture.numTouchesRequired = 1;

        _oneFingerSwipeDownGesture= new SwipeGesture(this);
        _oneFingerSwipeDownGesture.direction = SwipeGestureDirection.DOWN;
        _oneFingerSwipeDownGesture.numTouchesRequired = 1;

        _twoFingerSwipeDownGesture = new SwipeGesture(this);
        _twoFingerSwipeDownGesture.direction = SwipeGestureDirection.DOWN;
        _twoFingerSwipeDownGesture.numTouchesRequired = 2;

        _twoFingerSwipeLeftGesture = new SwipeGesture(this);
        _twoFingerSwipeLeftGesture.direction = SwipeGestureDirection.LEFT;
        _twoFingerSwipeLeftGesture.numTouchesRequired = 2;

        _oneFingerTapGesture = new TapGesture(this);
        _oneFingerTapGesture.numTouchesRequired = 1;

        _twoFingerTapGesture = new TapGesture(this);
        _twoFingerTapGesture.numTouchesRequired = 2;

        _threeFingerTapGesture = new TapGesture(this);
        _threeFingerTapGesture.numTouchesRequired = 3;

        _fourFingerTapGesture = new TapGesture(this);
        _fourFingerTapGesture.numTouchesRequired = 4;

        _pinchInGesture = new ZoomGesture(this);
        _pinchInGesture.slop = Math.round(20 / 50 * flash.system.Capabilities.screenDPI);

        addGestureListeners();
    }

    private function addGestureListeners():void {
        _oneFingerSwipeLeftGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerSwipeLeft, false, 0, true);
        _oneFingerSwipeRightGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerSwipeRight, false, 0, true);
        _oneFingerSwipeUpGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerSwipeUp, false, 0, true);
        _oneFingerSwipeDownGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerSwipeDown, false, 0, true);
        _twoFingerSwipeDownGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTwoFingerSwipeDown, false, 0, true);
        _oneFingerTapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerTap, false, 0, true);
        _twoFingerTapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTwoFingerTap, false, 0, true);
        _threeFingerTapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onThreeFingerTap, false, 0, true);
        _fourFingerTapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onFourFingerTap, false, 0, true);
        _pinchInGesture.addEventListener(GestureEvent.GESTURE_CHANGED, onPinchIn, false, 0, true);
        _twoFingerSwipeLeftGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTwoFingerSwipeLeft, false, 0, true);

    }
    private function removeGestureListeners():void {
        _oneFingerSwipeLeftGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerSwipeLeft);
        _oneFingerSwipeRightGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerSwipeRight);
        _oneFingerSwipeUpGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerSwipeUp);
        _oneFingerSwipeDownGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerSwipeDown);
        _twoFingerSwipeDownGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onTwoFingerSwipeDown);
        _oneFingerTapGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerTap);
        _twoFingerTapGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onTwoFingerTap);
        _threeFingerTapGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onThreeFingerTap);
        _fourFingerTapGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onFourFingerTap);
        _pinchInGesture.removeEventListener(GestureEvent.GESTURE_CHANGED, onPinchIn);
        _twoFingerSwipeLeftGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onTwoFingerSwipeLeft);

    }

    //---------------------------------------------------------------
    //  Private methods
    //---------------------------------------------------------------

    private function onTwoFingerSwipeDown(event:GestureEvent):void {

    }

    private function onPinchIn(event:GestureEvent):void {
        _numberSelectors[_currentNumberSelector].removeCurrentDigit();
        _pinchInGesture.reset();
    }

    private function onFourFingerTap(event:GestureEvent):void {
       _numberSelectors[_currentNumberSelector].onDigitChange(4,_numberSelectors[_currentNumberSelector].digitToEdit)
    }

    private function onThreeFingerTap(event:GestureEvent):void {
        _numberSelectors[_currentNumberSelector].onDigitChange(3,_numberSelectors[_currentNumberSelector].digitToEdit)
    }

    private function onTwoFingerTap(event:GestureEvent):void {
        _numberSelectors[_currentNumberSelector].onDigitChange(2,_numberSelectors[_currentNumberSelector].digitToEdit)
    }

    public function onOneFingerTap(event:GestureEvent):void {
        _numberSelectors[_currentNumberSelector].onDigitChange(1,_numberSelectors[_currentNumberSelector].digitToEdit);
    }

    private function onOneFingerSwipeRight(event:GestureEvent):void {
        _numberSelectors[_currentNumberSelector].moveDigitsRight();
    }

    private function onOneFingerSwipeLeft(event:GestureEvent):void {
        _numberSelectors[_currentNumberSelector].moveDigitsLeft();
    }

    private function onOneFingerSwipeUp(event:GestureEvent):void {
        _numberSelectors[_currentNumberSelector].onDigitChange(1,_numberSelectors[_currentNumberSelector].digitToEdit);

    }

    private function onOneFingerSwipeDown(event:GestureEvent):void {
        _numberSelectors[_currentNumberSelector].onDigitChange(-1,_numberSelectors[_currentNumberSelector].digitToEdit);

    }

    public function reverseOneFingerSwipeLeft(newFunction:Function):void {
        _oneFingerSwipeLeftGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, newFunction);
        _oneFingerSwipeLeftGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerSwipeLeft, false, 0, true);
    }

    public function editOneFingerSwipeLeft(newFunction:Function):void {
        _oneFingerSwipeLeftGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerSwipeLeft);
        _oneFingerSwipeLeftGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, newFunction, false, 0, true);
    }

    public function reverseOneFingerSwipeRight(newFunction:Function):void {
        _oneFingerSwipeRightGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, newFunction);
        _oneFingerSwipeRightGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerSwipeRight, false, 0, true);
    }

    public function editOneFingerSwipeRight(newFunction:Function):void {
        _oneFingerSwipeRightGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerSwipeRight);
        _oneFingerSwipeRightGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, newFunction, false, 0, true);
    }

    public function reverseOneFingerTap(newFunction:Function):void {
        _oneFingerTapGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, newFunction);
        _oneFingerTapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerTap, false, 0, true);
    }

    public function editOneFingerTap(newFunction:Function):void {
        _oneFingerTapGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onOneFingerTap);
        _oneFingerTapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, newFunction, false, 0, true);
    }

    private function onTwoFingerSwipeLeft(event:GestureEvent):void {

        twoFingerSwipeLeft.dispatch();
    }


}
}
