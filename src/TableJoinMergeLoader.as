package {
import flash.geom.Rectangle;

import me.gv3.alacarte.components.view.TableJoinMerge;

import starling.core.Starling;
import starling.textures.Texture;

public class TableJoinMergeLoader extends LoaderBase
{

    public function TableJoinMergeLoader()
    {
        super();
    }

   override protected function onGPUReady():void {
        var texture:Texture = Assets.getTexture("NumberSelectorBoxBg");
        var altTexture:Texture = Assets.getTexture("NumberSelectorBoxRedBg");
        var fontName:String = Assets.getFont("PartyCount").name;
        var rect:Rectangle = new Rectangle( 55,55, 138, 138);
        TableJoinMerge(Starling.current.root).setTextures(texture, rect, altTexture);
        TableJoinMerge(Starling.current.root).init("table",fontName,0x999999,72, "description 3/4");
        //TableJoinMerge(Starling.current.root).setToMergeSwap(33);
    }

}
}
