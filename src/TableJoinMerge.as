package {


import flash.geom.Rectangle;
import flash.text.AntiAliasType;
import flash.text.TextField;


import com.greensock.TweenLite;

import flash.text.TextFieldAutoSize;

import flash.text.TextFormat;
import flash.text.TextFormatAlign;

import org.gestouch.events.GestureEvent;
import org.gestouch.gestures.LongPressGesture;
import org.gestouch.gestures.SwipeGesture;
import org.gestouch.gestures.SwipeGestureDirection;

import selectors.NumberSelector;

public class TableJoinMerge extends PartySelectionView{

    //---------------------------------------------------------------
    //  Public variables
    //---------------------------------------------------------------


    //---------------------------------------------------------------
    //  Private variables
    //---------------------------------------------------------------


    private var _oneFingerLongPressGesture:LongPressGesture;
    private var _oneFingerSwipeLeftGesture:SwipeGesture;
    private var _twoFingerSwipeLeftGesture:SwipeGesture;
    private var _action:Number;
    private var _merging:Boolean;
    private var _newLabelTextField:TextField;
    private var _newSubLabelTextField:TextField;


    public function TableJoinMerge() {
        super();
        initExtraGestures();
        _action = 0;
        _merging = false;
    }

    private function shake():void {
        var _lenght:Number = numberSelectors.length;
        for (var i:int = 0; i < _lenght; i++) {
             numberSelectors[i].shakeTween();
        }
    }

    private function stopShake():void {
        removeTap(stopShake);
        editTap(onOneFingerTap);
        var _lenght:Number = numberSelectors.length;
        for (var i:int = 0; i < _lenght; i++) {
            numberSelectors[i].stopShake();
        }
    }

    private function newTable(animate:Boolean = true):void {
        var duration:Number;
        if (animate)
           duration = 0.5;
        else
            duration = 0;
        var newNumberSelector:NumberSelector = new NumberSelector(newBackground(),new Rectangle( 55,55, 138, 138),_font);
        newNumberSelector.x = 1136;
        newNumberSelector.y = this.height/3 - newNumberSelector.height/2;
        newNumberSelector.centralised = true;
        addChild(newNumberSelector);
        numberSelectors.push(newNumberSelector);
        numberSelectors[currentNumberSelector].isCurrentNumberSelector = false;
        TweenLite.to(numberSelectors[currentNumberSelector], duration, {x: 284 - (numberSelectors[currentNumberSelector].width/2)});
        TweenLite.to(titleTextField, duration, {x: 284 - (titleTextField.width/2)});
        TweenLite.to(subTitleTextField, duration, {x: 284 - (subTitleTextField.width/2)});
        currentNumberSelector++;

        TweenLite.to(numberSelectors[currentNumberSelector], duration, {x: 852 - (numberSelectors[currentNumberSelector].width/2)});

        _newLabelTextField = new TextField();
        _newLabelTextField.defaultTextFormat = format;
        _newLabelTextField.antiAliasType = AntiAliasType.ADVANCED;
        _newLabelTextField.condenseWhite = true;
        _newLabelTextField.autoSize = TextFieldAutoSize.CENTER;
        _newLabelTextField.y = this.height - 250;
        _newLabelTextField.alpha = 0.7;
        _newLabelTextField.text = "table";
        _newLabelTextField.x = 1136;
        addChild(_newLabelTextField);

        _newSubLabelTextField = new TextField();
        _newSubLabelTextField.y = this.height - 150;
        _newSubLabelTextField.defaultTextFormat = formatSub;
        _newSubLabelTextField.antiAliasType = AntiAliasType.ADVANCED;
        _newSubLabelTextField.condenseWhite = true;
        _newSubLabelTextField.autoSize = TextFieldAutoSize.CENTER;
        _newSubLabelTextField.alpha = 0.7;
        _newSubLabelTextField.text = "description";
        _newSubLabelTextField.x = 1136;
        addChild(_newSubLabelTextField);

        TweenLite.to(_newSubLabelTextField, duration, {x:852 - _newSubLabelTextField.width/2});
        TweenLite.to(_newLabelTextField, duration, {x:852 - _newLabelTextField.width/2, onComplete:shake});

    }

    private function newLabel(value:String):void {
        _newLabelTextField.text =  value.toString();
        _newLabelTextField.x = 852 - _newLabelTextField.width >>1;
    }


    private function changeAction(direction:Number):void {
        var numberSelector:NumberSelector = numberSelectors[currentNumberSelector];
        if (direction == 0 && _action < 2) {
            numberSelector.expand();
            _action++;
        }
        else if (direction == 0 && _action == 2) {
            numberSelector.expand();
            _action = 0;
        }
        else if (direction == 1 && _action < 2) {
            numberSelector.expand();

            _action--;
        }
        else if (direction == 1 && _action == 2) {
            numberSelector.expand();
            _action = 1;
        }

    }

    public function setToMergeSwap(tableNumber:Number):void {
        numberSelectors[currentNumberSelector].setPartyCount(tableNumber);
        mergeSwapTable(null,false);
        editTap(onTap);
    }

    private function initExtraGestures():void {
        _oneFingerLongPressGesture = new LongPressGesture(this);
        _oneFingerLongPressGesture.numTouchesRequired = 1;

        _twoFingerSwipeLeftGesture= new SwipeGesture(this);
        _twoFingerSwipeLeftGesture.direction = SwipeGestureDirection.LEFT;
        _twoFingerSwipeLeftGesture.numTouchesRequired = 2;

        _oneFingerSwipeLeftGesture= new SwipeGesture(this);
        _oneFingerSwipeLeftGesture.direction = SwipeGestureDirection.LEFT;
        _oneFingerSwipeLeftGesture.numTouchesRequired = 1;

        addGestureListeners();
    }

    private function addGestureListeners():void {
        _oneFingerLongPressGesture.addEventListener(GestureEvent.GESTURE_BEGAN, onOneFingerLongPress, false, 0, true);
    }
    private function removeGestureListeners():void {
        _oneFingerLongPressGesture.removeEventListener(GestureEvent.GESTURE_BEGAN, onOneFingerLongPress);
    }

    private function onOneFingerLongPress(event:GestureEvent):void {
        if (!numberSelectors[currentNumberSelector].isShakeing && !numberSelectors[currentNumberSelector].animating && !_merging) {
            shake();
            editSwipeLeft(mergeSwapTable);
            editTap(onTap);
        }
       // else if (numberSelectors[currentNumberSelector].isShakeing && numberSelectors[currentNumberSelector].animating) {
       //     stopShake();


       // }
    }

    private function mergeSwapTable(event:GestureEvent = null, animate:Boolean = true):void {
            numberSelectors[currentNumberSelector].centralise(animate);
            newTable(animate);
            _merging = true;
            removeSwipeLeft(mergeSwapTable);
            editSwipeLeft(changeActionOnSwipeLeft);
            editSwipeRight(changeActionOnSwipeRight);
    }

    private function onTap(event:GestureEvent):void {
        stopShake();
        if (_merging) {
            removeSwipeLeft(changeActionOnSwipeLeft);
            removeSwipeRight(changeActionOnSwipeRight);
        }
        else
            removeSwipeLeft(mergeSwapTable);
        removeTap(onTap);

    }

    private function changeActionOnSwipeLeft(event:GestureEvent):void {
        changeAction(0);
    }
    private function changeActionOnSwipeRight(event:GestureEvent):void {
        changeAction(1);
    }

    public function removeSwipeLeft(func:Function):void {
        reverseOneFingerSwipeLeft(func);
    }

    public function editSwipeLeft(func:Function):void {
        editOneFingerSwipeLeft(func);
    }

    public function removeSwipeRight(func:Function):void {
        reverseOneFingerSwipeRight(func);
    }

    public function editSwipeRight(func:Function):void {
        editOneFingerSwipeRight(func);
    }

    public function removeTap(func:Function):void {
        reverseOneFingerTap(func);
    }

    public function editTap(func:Function):void {
        editOneFingerTap(func);
    }



}
}
