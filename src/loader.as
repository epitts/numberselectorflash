/**
 * Created with IntelliJ IDEA.
 * User: Elliott
 * Date: 9/15/13
 * Time: 6:41 PM
 * To change this template use File | Settings | File Templates.
 */
package {
import flash.desktop.NativeApplication;
import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.text.Font;
import flash.text.StyleSheet;


[SWF (frameRate="60", backgroundColor="#333333")]
public class loader extends Sprite {

    [Embed(source="../media/fonts/HelveticaNeueUltraLight.ttf",
            fontName="HelveticaNeueUltraLight",
            fontFamily="HelveticaNeue",
            advancedAntiAliasing="true",
            embedAsCFF="false")]
    var HelveticaNeueUltraLight:Class;

    [Embed(source="../media/fonts/alacarteWaiterStyle.css", mimeType="application/octet-stream")]
    private var stylesCSS:Class;

    public var _css:StyleSheet;



    public function loader() {
        super();
        this.stage.scaleMode = StageScaleMode.NO_SCALE;
        this.stage.align = StageAlign.TOP_LEFT;

        NativeApplication.nativeApplication.executeInBackground = true;


        if(this.stage)
        {
            onAddedToStage();
        }

        else addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);

    }

    public function createStyleSheetObject():StyleSheet
    {
        // create a new StyleSheet instance
        var styleSheet:StyleSheet = new StyleSheet();
        styleSheet.parseCSS(new stylesCSS().toString());
        return styleSheet;
    }

    private function onAddedToStage(event:Event = null):void {
        _css = createStyleSheetObject();
        var font:Font = new HelveticaNeueUltraLight;
       // Font.registerFont(HelveticaNeueUltraLight);
        var tableJoingmerge:TableJoinMerge = new TableJoinMerge();
        tableJoingmerge.init("guest",_css, "description");
        addChild(tableJoingmerge);
    }
}
}
